import type { StrictUseAxiosReturn, UseAxiosOptions } from '@vueuse/integrations/useAxios'
import { useAxios as useAxiosApi } from '@vueuse/integrations/useAxios'
import type { AxiosRequestConfig } from 'axios'
import axios from 'axios'
import qs from 'qs'


const axiosBaseConfig: AxiosRequestConfig = {
  baseURL: '/',
  // params格式化规则
  paramsSerializer: {
    serialize: (params) => qs.stringify(params, { arrayFormat: 'repeat' }),
  },
  headers: {
    'Content-Type': 'application/json',
  },
}
const instance = axios.create(axiosBaseConfig)

// response拦截
instance.interceptors.response.use((response) => {

  return response.data
})

export const useAxios = <T extends ResponseBody.Default<any>, R = any, D = any>(
  url: string,
  config: Omit<AxiosRequestConfig, 'url'> = {},
  options: UseAxiosOptions<T> = {
    immediate: true,
  },
): StrictUseAxiosReturn<T['data'], R, D> & PromiseLike<StrictUseAxiosReturn<T['data'], R, D>> => {
  return useAxiosApi<T>(url, config, instance, options) as any
}

export function request<T = any>(
  url: string,
  options: Omit<AxiosRequestConfig, 'url'> = { method: 'get' },
): Promise<T> {
  return instance({
    url,
    ...options,
  }) as any
}

export default request

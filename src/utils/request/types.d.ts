/** 全局接口返回格式 */
declare namespace ResponseBody {
  export interface Default<T> {
    code?: '0' | '1'
    data: T
    message: string
    errors?: string[]
  }

  type Pagination<T> = ResponseBody.Default<{
    records: T[]
    current: number
    size: number
    total: number
  }>
}

interface PaginationParamsBase {
  current?: number
  size?: number
}

type PaginationParams<T extends Record<string, any> = {}> = PaginationParamsBase &
  Omit<T, keyof PaginationParamsBase>

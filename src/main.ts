import axios from 'axios'
import 'normalize.css'
import { createPinia } from 'pinia'
import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import router from './router'
import 'ant-design-vue/dist/antd.less'

const app = createApp(App)

axios.defaults.withCredentials = true
app.config.globalProperties.$axios = axios

const pinia = createPinia()

app.use(router)
app.use(pinia)

app.mount('#app')

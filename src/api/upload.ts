import { request } from '~/utils/request'

/**
 * 上传图片
 * @param data
 * @returns
 */
export function upload(
  data: PaginationParams<{
    image: string
  }>,
) {
  return request<{ results: any[] }>(`/api`, {
    method: 'POST',
    data,
  })
}

export function getBase64(file: File) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = (error) => reject(error)
  })
}

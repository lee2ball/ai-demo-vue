# 物体检测demo前端

## 开发

```
pnpm install
pnpm start
```

## 静态打包

```
pnpm build
```

## 预览

```
pnpm preview
```

## DOCKER 部署

```
docker build -t ai-demo-fe:latest .
```

### 环境变量

| Key  | Description  |
|---|---|
| AI_SERVER_HOST | 后端服务地址 |

